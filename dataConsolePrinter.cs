using System;
using System.Collections.Generic;
class DataConsolePrinter {
    public void Print(IDataset dataset) {
        var data=dataset.GetData();
        Console.WriteLine("[");
        if (data != null) {
            foreach (List<string> list in data) {
                Console.Write("[ ");
                foreach (string currentCell in list)
                    Console.Write("\""+currentCell+"\" ");
                Console.WriteLine("]");
            }
        }
        Console.WriteLine("]");
    }
}
