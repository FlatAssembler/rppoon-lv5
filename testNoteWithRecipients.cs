class TestNoteWithRecipients {
    public static void Main() {
        var note=new NoteWithRecipients("Why do you keep bothering us with such time-consuming and pointless tasks?",new DarkTheme());
        note.AddRecipient("Professors");
        note.AddRecipient("Employers");
        note.AddRecipient("Assistants");
        note.RemoveRecipient("Employers");
        note.Show();
        note=new NoteWithRecipients("Do you really think we'll learn something useful that way?",new LightTheme());
        note.AddRecipient("Professors");
        note.AddRecipient("Assistants");
        note.Show();
    }
}
