using System;
using System.Text;
using System.Collections.Generic;
class Box : IBillable,IShippable
{
    private List<IBillable> items;
    private string name;
    private double weightOfTheBox;
    public Box(string name, double weight) {
        this.items = new List<IBillable>();
        this.name = name;
        this.weightOfTheBox=weight;
    }
    public void Add(IBillable item) {
        this.items.Add(item);
    }
    public void Remove(IBillable item) {
        this.items.Remove(item);
    }
    public double Price {
        get {
            double totalPrice = 0;
            foreach (IBillable item in items) {
                totalPrice += item.Price;
            }
        return totalPrice;
        }
    }
    public string Description(int depth = 0) {
        StringBuilder builder =
        new StringBuilder(new string(' ', depth) + this.name + "\n");
        foreach (IBillable item in items) {
            builder.Append(item.Description(depth + 2)).Append("\n");
        }
        return builder.ToString();
    }
    public double Weight { get {
            double totalWeight=weightOfTheBox;
            foreach (IShippable item in items)
                totalWeight+=item.Weight;
            return totalWeight;
        }
    }
}
