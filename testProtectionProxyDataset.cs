class TestProtectionProxyDataset {
    public static void Main() {
        User[] users=new User[3];
        for (int i=0; i<3; i++)
            users[i]=User.GenerateUser("User"+i);
        var proxies=new System.Collections.Generic.List<IDataset>();
        for (int i=0; i<3; i++)
            proxies.Add(new ProtectionProxyDataset(users[i]));
        var printer=new DataConsolePrinter();
        for (int i=0; i<3; i++) {
            System.Console.WriteLine("The data delivered to the user #" + i + ":");
            printer.Print(proxies[i]);
        }
    }
}
