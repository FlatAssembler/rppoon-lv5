using System;
using System.Collections.Generic;
class NoteWithRecipients : Note {
    private List<string> recipients;
    public NoteWithRecipients(string message, ITheme theme) : base(message, theme) {
        recipients=new List<string>();
    }
    public void AddRecipient(string newRecipient) {
        if (!recipients.Contains(newRecipient))
            recipients.Add(newRecipient);
        else
            throw new Exception("Recipient named \""+newRecipient+"\" has already been added.");
    }
    public void RemoveRecipient(string oldRecipient) {
        if (!recipients.Remove(oldRecipient))
            throw new Exception("Recipient named \""+oldRecipient+"\" can't be removed from the list.");
    }
    public override void Show()
    {
        this.ChangeColor();
        Console.Write("MESSAGE TO: ");
        foreach (string recipient in recipients)
            Console.Write("\""+recipient+"\" ");
        Console.WriteLine("");
        string framedMessage = this.GetFramedMessage();
        Console.WriteLine(framedMessage);
        Console.ResetColor();
    }
}
