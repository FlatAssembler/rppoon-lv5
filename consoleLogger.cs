class ConsoleLogger {
    private static ConsoleLogger instance;
    private ConsoleLogger() {}
    public static ConsoleLogger GetInstance() {
        if (instance==null)
            instance=new ConsoleLogger();
        return instance;
    }
    public void Log(string message) {
        System.Console.WriteLine("Logged: "+message);
    }
}
