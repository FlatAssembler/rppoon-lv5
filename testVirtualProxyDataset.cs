class TestVirtualProxyDataset {
    public static void Main() {
        DataConsolePrinter printer = new DataConsolePrinter();
        IDataset dataset=new VirtualProxyDataset("example.csv");
        string[] multiplicationTable=new string[10];
        for (int i=0; i<10; i++)
            for (int j=0; j<10; j++)
                multiplicationTable[i]+=(i+1)*(j+1)+((j<9)?(","):(""));
        System.IO.File.WriteAllLines("example.csv",multiplicationTable);
        printer.Print(dataset);
    }
}
