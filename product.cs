class Product : IShippable,IBillable
{
    private double price;
    private string description;
    private double weight;
    public Product(string description, double weight, double price) {
        this.description = description;
        this.price = price;
        this.weight = weight;
    }
    public double Price { get { return this.price; } }
    public double Weight { get {return this.weight; } }
    public string Description(int depth = 0) {
        return new string(' ', depth) + this.description;
    }
}
