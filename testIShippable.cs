using System;
using System.Collections.Generic;
class TestIShippable {
    public static void Main(){
        List<IShippable> list=new List<IShippable>();
        Product graphicCard=new Product("Graphics card", 5, 10);
        Product processor=new Product("Processor", 5, 20);
        Product motherBoard=new Product("Motherboard", 10, 10);
        Box computer=new Box("Computer",20);
        computer.Add(graphicCard);
        computer.Add(processor);
        computer.Add(motherBoard);
        list.Add(computer);
        Box car=new Box("Car",100);
        Product engine=new Product("Engine", 50, 100);
        Product accumulator=new Product("Accumulator", 25, 50);
        Product wheel=new Product("Wheel", 25, 10);
        car.Add(engine);
        car.Add(accumulator);
        for (int i=0; i<4; i++)
            car.Add(wheel);
        list.Add(car);
        Product book=new Product("Book",5,2);
        list.Add(book);
        foreach (IShippable item in list)
            Console.WriteLine(item.Description());
    }
}
