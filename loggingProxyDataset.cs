using System.Collections.ObjectModel;
using System.Collections.Generic;

class LoggingProxyDataset : IDataset {
    private string filePath;
    private Dataset dataset;
    private ConsoleLogger logger;
    private int counter;
    public LoggingProxyDataset(string fileName) {
        this.filePath=fileName;
        logger=ConsoleLogger.GetInstance();
        logger.Log("LoggingProxyDataset created with filename \""+fileName+"\".");
        counter=0;
    }
    public ReadOnlyCollection<List<string>> GetData() {
        counter++;
        logger.Log("\"GetData\" called on a LoggingProxyDataset with filename \"" + filePath + "\" for the " + counter + "th time.");
        if (dataset == null)
        {
            logger.Log("Creating a dataset with filename \"" + filePath + "\".");
            dataset = new Dataset(filePath);
        }
        return dataset.GetData();
    }

}
