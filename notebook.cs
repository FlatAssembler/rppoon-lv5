using System;
using System.Collections.Generic;
class Notebook
{
    private List<Note> notes;
    private ITheme theme;
    public Notebook(ITheme theme=null) {
        this.notes = new List<Note>();
        this.theme=(theme==null)?(new LightTheme()):(theme);
    }
    public void AddNote(Note note) {
        note.Theme=this.theme;
        this.notes.Add(note);
    }
    public void ChangeTheme(ITheme theme) {
        foreach (Note note in this.notes) {
            note.Theme = theme;
        }
    }
    public void Display() {
        foreach (Note note in this.notes) {
            note.Show();
            Console.WriteLine("\n");
        }
    }
}
