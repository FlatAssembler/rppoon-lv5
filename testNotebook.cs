class TestNotebook {
    public static void Main(){
        var notebook=new Notebook(new DarkTheme());
        var note=new NoteWithRecipients("Why do you keep bothering us with such time-consuming and pointless tasks?",new DarkTheme());
        note.AddRecipient("Professors");
        note.AddRecipient("Employers");
        note.AddRecipient("Assistants");
        note.RemoveRecipient("Employers");
        notebook.AddNote(note);
        note=new NoteWithRecipients("Do you really think we'll learn something useful that way?",new LightTheme());
        note.AddRecipient("Professors");
        note.AddRecipient("Assistants");
        notebook.AddNote(note);
        notebook.Display();
    }
}
