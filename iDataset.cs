using System.Collections.ObjectModel;
using System.Collections.Generic;
interface IDataset
{
        ReadOnlyCollection<List<string>> GetData();
}
